import { defineStore } from 'pinia'

export const store = defineStore('store', {
  state: () => ({
    counters: [
      { count: 20, backgroundColor: '#fab700' },
      { count: 20, backgroundColor: '#3e82cd' },
      { count: 20, backgroundColor: '#aa3ecd' },
      { count: 20, backgroundColor: '#5fe386' },
      { count: 20, backgroundColor: '#e35fcd' },
      { count: 20, backgroundColor: '#e3865f' },
    ],
    columns: 1,
    size: 2,
    mirrored: true,
    openSettingsMenu: false,
  }),
  getters: {},
  actions: {
    increase(id, amount) {
      this.counters[id].count += amount
    },
    toggleSettingsMenu() {
      this.openSettingsMenu = !this.openSettingsMenu
    },
    toggleMirror() {
      this.mirrored = !this.mirrored
    },
    saveSettings(options){
      this.mirrored = options['mirrored']
      this.columns = options['columns']
      this.size = options['size']
    }
  },
})
